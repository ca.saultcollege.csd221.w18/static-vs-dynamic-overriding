/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.staticvsdynamicoverride;

/**
 *
 * @author rod
 */
public class Super {
    public void printDynamic() { 
        System.out.println("Dynamic message from Super");
    }
    
    public static void printStatic() { 
        System.out.println("Static message from Super");
    }
}
