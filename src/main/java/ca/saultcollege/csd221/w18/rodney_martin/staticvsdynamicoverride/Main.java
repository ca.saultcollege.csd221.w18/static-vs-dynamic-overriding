/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.rodney_martin.staticvsdynamicoverride;

/**
 *
 * @author rod
 */
public class Main {
   
    public static void main( String[] args ) {
        
        // When we instantiate each class using its own class as the object's data type, then things seem fairly normal
        Super super1 = new Super();
        Sub sub1 = new Sub();
        super1.printStatic();   // outputs 'Static message from Super'
        super1.printDynamic();  // outputs 'Dynamic message from Super'
        sub1.printStatic();     // outputs 'Static message from Sub'
        sub1.printDynamic();    // outputs 'Dynamic message from Sub'

        // But when we use the super class as the data type for both, then things get a bit more interesting
        Super sub2 = new Sub(); // sub2 is a Sub, but its data type is Super
        
        // outputs 'Static message from Super'  
        // NOTE: this is the message from the super class!
        // Since the compiler sees sub2 as an instance of Super, 
        // it will use Super's static method
        // This is a case of "Static binding": the choice of which
        // printStatic method to use is made at compile time
        sub2.printStatic();

        // outputs 'Dynamic message from Sub'
        // Here we have true Polymorphism in action, an instance of 'Dynamic
        // binding'.  The choice of which printDynamic method to use is made
        // at runtime.  The runtime environment knows that sub2 is really
        // an instance of Sub, so it uses the printDynamic method from Sub
        sub2.printDynamic();   
    }
    
}
